#!/bin/bash

# Check if a package is installed
check_installed() {
	if rpm -qi $1 > /dev/null 2>& 1; then
		return 0 # Return status 0 for true
	else
		return 1 # Return status 1 for false
	fi
}

# Ask to install a package if it isn't already
ask_to_install() {
	if ! check_installed $1; then
		while true; do
			read -p "$1 was not detected! Install now [Y/n]?" yn
			case $yn in
				[Yy]* ) sudo dnf install $1; break;;
				[Nn]* ) return 1;;
			esac
		done
		return 0
	else
		echo "$1 is detected."
		return 0
	fi
}

spec="budgie-applications-menu.spec"

if [ ! -f $spec ]; then
	echo "$spec is missing from working directory. Cannot build without it."
	exit
fi

if ! ask_to_install fedora-packager; then
	echo "Cannot proceed without fedora-packager. Aborting..."
	exit
fi

mkdir -p ./build/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS} # Setup package build environment
mkdir -p ./rpms # Built package moved to here

cp $spec ./build/SPECS/$spec
cd build
spec="./SPECS/$spec"

# Get version number in spec file
version="$(cat $spec | grep -oP "Version:\s*\K.*")"
# Get full commit ID
commit="$(cat $spec | grep -oP "%global commit0\s*\K.*")"

# Install build dependencies for package spec
echo "This requires sudo permissions to install the package build dependencies."
sudo dnf builddep $spec

# Grab required source files
wget -O ./SOURCES/applications-menu-$commit.zip https://github.com/ubuntubudgie/applications-menu/archive/$commit.zip

# Build the RPM file
rpmbuild --define "_topdir $(pwd)" --define "debug_package %{nil}" -v -bb $spec

# Move built RPM file to rpm dir
mv ./RPMS/x86_64/*.rpm ./../rpms