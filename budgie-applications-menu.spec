%global commit0 4aaba6436ba368c155456613016613b16374c98e
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global repo_name applications-menu

Name: budgie-applications-menu
Version: %{shortcommit0}
Release: 1%{?dist}
License: GPL-3.0
Summary: A feature rich applications menu for Budgie Desktop
URL: https://github.com/ubuntubudgie/applications-menu

Source0: https://github.com/ubuntubudgie/%{repo_name}/archive/%{repo_name}-%{commit0}.zip

BuildRequires: appstream-devel
BuildRequires: libgee-devel
BuildRequires: gnome-menus-devel
BuildRequires: granite-devel
BuildRequires: gtk3-devel
BuildRequires: libhandy-devel
BuildRequires: json-glib-devel
BuildRequires: plank-devel
BuildRequires: libsoup-devel
BuildRequires: switchboard-devel
BuildRequires: libunity-devel
BuildRequires: wingpanel-devel
BuildRequires: libzeitgeist-devel

BuildRequires: meson
BuildRequires: vala
BuildRequires: intltool

#Requires: glib2
Requires: granite
Requires: libgee
#Requires: gtk3
#Requires: gdk-pixbuf2
Requires: json-glib
Requires: zeitgeist
Requires: gnome-menus
Requires: plank-libs
Requires: libpeas
Requires: budgie-desktop
Requires: libhandy
#Requires: glibc

%description
budgie-applications-menu provides a feature rich menu to launch applications and access power options for Budgie Desktop

%prep
%setup -q -n %{repo_name}-%{commit0}

%build
meson build --prefix=/usr
cd build
ninja

%install
cd build
%ninja_install

%files
%{_exec_prefix}/lib64/budgie-desktop/plugins/applications-menu/AppMenu.plugin
%{_exec_prefix}/lib64/budgie-desktop/plugins/applications-menu/libslingshot.so
%{_datadir}/glib-2.0/schemas/io.elementary.desktop.wingpanel.applications-menu.gschema.xml
%{_datadir}/glib-2.0/schemas/org.ubuntubudgie.plugins.budgie-appmenu.gschema.xml
%{_datadir}/locale/aa/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ab/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ae/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/af/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ak/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/am/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/an/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ar/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/as/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ast/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/av/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ay/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/az/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ba/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/be/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/bg/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/bh/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/bi/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/bm/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/bn/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/bo/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/br/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/bs/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ca/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ca@valencia/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ce/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ch/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ckb/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/co/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/cr/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/cs/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/cu/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/cv/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/cy/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/da/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/de/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/dv/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/dz/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ee/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/el/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/en_AU/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/en_CA/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/en_GB/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/eo/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/es/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/et/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/eu/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/fa/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ff/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/fi/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/fil/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/fj/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/fo/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/fr/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/fr_CA/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/fy/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ga/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/gd/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/gl/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/gn/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/gu/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/gv/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ha/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/he/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/hi/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ho/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/hr/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ht/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/hu/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/hy/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/hz/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ia/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/id/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ie/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ig/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ii/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ik/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/io/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/is/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/it/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/iu/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ja/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/jv/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ka/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/kg/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ki/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/kj/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/kk/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/kl/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/km/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/kn/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ko/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/kr/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ks/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ku/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/kv/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/kw/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ky/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/la/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/lb/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/lg/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/li/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ln/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/lo/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/lt/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/lu/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/lv/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/mg/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/mh/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/mi/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/mk/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ml/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/mn/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/mo/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/mr/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ms/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/mt/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/my/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/na/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/nb/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/nd/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ne/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ng/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/nl/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/nn/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/no/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/nr/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/nv/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ny/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/oc/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/oj/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/om/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/or/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/os/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/pa/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/pi/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/pl/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ps/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/pt/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/pt_BR/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/qu/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/rm/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/rn/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ro/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ru/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/rue/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/rw/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/sa/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/sc/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/sd/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/se/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/sg/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/si/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/sk/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/sl/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/sm/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/sma/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/sn/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/so/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/sq/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/sr/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/sr@latin/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ss/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/st/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/su/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/sv/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/sw/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/szl/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ta/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/te/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/tg/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/th/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ti/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/tk/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/tl/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/tn/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/to/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/tr/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ts/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/tt/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/tw/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ty/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ug/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/uk/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ur/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/uz/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/ve/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/vi/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/vo/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/wa/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/wo/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/xh/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/yi/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/yo/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/za/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/zh/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/zh_CN/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/zh_HK/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/zh_TW/LC_MESSAGES/slingshot.mo
%{_datadir}/locale/zu/LC_MESSAGES/slingshot.mo
